﻿using System;
using System.Globalization;
using Neurorehab.Scripts.Devices.Abstracts;
using Neurorehab.Scripts.Devices.Data;
//using Neurorehab.Scripts.Plotter;
using UnityEngine;
using UnityEngine.UI;

    /// <summary>
    /// Responsible for updating the Unity Bitalino data according to its <see cref="BitalinoData"/>.
    /// </summary>
public class BitalinoUnity : MonoBehaviour
{
    [Header("Settings")]
    public Text A1Value;
    public Text A2Value;
    public Text A3Value;
    public Text A4Value;
    public Text A5Value;
    public Text A6Value;
    public Text D1Value;
    public Text D2Value;
    public Text BatValue;

    [Header("GUI")]
    public Text IdValue;

    private BitalinoReader reader;
    private BitalinoManager manager;
    private ConnectionState connectionState;

    public InputField IPSend, PortSend;

    public Button StartButton, StopButton;

    /// <summary>
    /// Initializes all the plotters
    /// </summary>
    private void Start()
    {
        reader = GetComponent<BitalinoReader>();
        manager = GetComponent<BitalinoManager>();
        StartButton.interactable = false;
        IdValue.text = "BITALINO"; //+ BitalinoManager.device;
    }

    public void UDPClient()
    {
        if (int.TryParse(PortSend.text, out UDPDataSend.Port) && IPSend.text != "")
        {
            UDPDataSend.Ip = IPSend.text;
            StartButton.interactable = true;
        }
        else
            StartButton.interactable = false;
    }
        
    private void Update()
    {
        UpdateGuiValues();
    }

    public void ClientSend()
    {
        if (int.TryParse(PortSend.text, out UDPDataSend.Port) && IPSend.text != "")
        {
            UDPDataSend.Ip = IPSend.text;
            StartButton.interactable = true;
        }
        else
            StartButton.interactable = false;

        //UDPDataSend.Port = int.Parse(PortSend.text);
        //UDPDataSend.Ip = IPSend.text;
        if (!UDPDataSend.IsConnected)
        {
            UDPDataSend.Init();
            BitalinoManager.stringText = "Starting UDP Send";
        }

    }

    public void CloseApplication()
    {
        Application.Quit();
    }

    public void ClientStop()
    {
        UDPDataSend.CloseClient();
    }

    /// <summary>
    /// Updates the values of each bioplux signal in the GUI according to its <see cref="GenericDeviceUnity.GenericDeviceData"/> 
    /// </summary>
    private void UpdateGuiValues()
    {

        if (reader.getBuffer() != null)
        {
            foreach (BITalinoFrame f in reader.getBuffer())
            {
                if (f != null)
                {
                    for (int i = 0; i < manager.analogAndChannels.Length; i++)
                    {
                        string multiString = "[$]trackingType|id=" + DateTime.Now.Ticks + " | Analog,";

                        multiString += "[$$]BITalino," + "[$$$]" + manager.analogAndChannels[i].analog + ",value," + f.GetAnalogValue(i).ToString(new CultureInfo("en-US")) + ";";

                        UDPDataSend.SendString(multiString);
                        Debug.Log(multiString);
                    }

                    for(int i = 0; i < 4; i++)
                    {
                        string x = "[$]trackingType|id=" + DateTime.Now.Ticks + " | Digital,[$$]BITalino,[$$$]D" + i.ToString() + ",value," + f.GetDigitalValue(i).ToString(new CultureInfo("en-US")) + ";";

                        UDPDataSend.SendString(x);
                        
                    }

                    A1Value.text = "A1: " + f.GetAnalogValue(0).ToString();

                    A2Value.text = "A2: " + f.GetAnalogValue(1).ToString();

                    A3Value.text = "A3: " + f.GetAnalogValue(2).ToString();

                    A4Value.text = "A4: " + f.GetAnalogValue(3).ToString();

                    A5Value.text = "A5: " + f.GetAnalogValue(4).ToString();

                    A6Value.text = "A6: " + f.GetAnalogValue(5).ToString();

                    D1Value.text = "D1: " + f.GetDigitalValue(0).ToString();

                    D2Value.text = "D2: " + f.GetDigitalValue(1).ToString();
                }
            }
        }
    }
}