using UnityEngine;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System;
using UnityEngine.UI;

public class UDPDataSend : MonoBehaviour
{
    public static string Ip;
    public static int Port;
    public static bool IsConnected;
	
    private static IPEndPoint _endPoint;
    private static UdpClient _client;

    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public static void Init()
    { 
        _endPoint = new IPEndPoint(IPAddress.Parse(Ip), Port);
        _client = new UdpClient();
        IsConnected = true;
        Debug.Log("Starting udp send");
        
    }
	//TODO: REMEMBER to add a pop up message when trying to send string
    public static void SendString(string message)
    {
        try
        {
            if (message != "")
            {
                // UTF8 encoding to binary format.
                byte[] data = Encoding.UTF8.GetBytes(message);
                // Send the message to the remote Client.
                _client.Send(data, data.Length, _endPoint);
                //Debug.Log("making sure it's being sent: " + message);
            }
        }

        catch (Exception err)
        {
          Debug.Log(err.ToString());
        }
    }

    public static void CloseClient()
    {
        if (IsConnected)
        {
            _client.Close();
            Debug.Log("Client Closed");
        }
    }

    private void OnApplicationQuit()
    {
        CloseClient();
    }
}
